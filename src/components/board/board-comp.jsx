import React from 'react'
import Square from '../square/square.comp'
import './board.styles.css'

class Board extends React.Component{

    renderSquare(i){
        return (
          <Square value = {this.props.squares[i]} onClick={() => this.props.onClick(i)}></Square>
        )
        
    }

    render() {
        const status = `Next player: ${(this.props.value) === true ? 'X' : 'O' }`;
    
        return (
          <div>
            <div className="status" style = {{fontSize : '20px'}}>{status}</div>
            <div className="board-row">
              {this.renderSquare(0)}
              {this.renderSquare(1)}
              {this.renderSquare(2)}
            </div>
            <div className="board-row">
              {this.renderSquare(3)}
              {this.renderSquare(4)}
              {this.renderSquare(5)}
            </div>
            <div className="board-row">
              {this.renderSquare(6)}
              {this.renderSquare(7)}
              {this.renderSquare(8)}
            </div>
          </div>
        )
    }
}
export default Board