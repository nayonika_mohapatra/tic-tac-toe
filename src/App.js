import React from 'react';
import './App.css';
import Game from './components/game/game.comp'

function App() {
  return (
    <div className="App">
      <Game></Game>
    </div>
  );
}

export default App;
